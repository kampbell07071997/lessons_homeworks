function checkPrimeNumbers(){
	let a = +document.getElementById('number1').value;
	let b = +document.getElementById('number2').value;
	let primes = '';
	let int;
	for (let i = a; i <= b; i++) {
		int = true;
		for (let j = 2; j < i && true; j++) {
			if (i % j === 0) {
				int = false;
			}
		} if (int && i!==1){
			primes += i + ' ';
		}
	}
	document.getElementById('resultOne').innerHTML = `Прості числа: ${primes}.`;
};
function sumNumbers(){
	let x = document.getElementById('number').value;
	let sum = 0;
	for (let i = 0; i < x.length; i++) {
		sum += Number(x[i]);
	}
	document.getElementById('resultTwo').innerHTML = `Сума чисел дорівнює - ${sum}`;
	return sum;
};
function sumNumbersNandM() {
	let n = +document.getElementById('numberN').value;
	let m = document.getElementById('numberM').value;
	let k = +document.getElementById('numberK').value;
	let sum2 = m[0];
	let sum1 = n % 10**k;
	for (i = 1; i < k; i++) {
		sum2 += m[i];
	}
	sum = sum1 + +sum2;
	document.getElementById('resultThree').innerHTML = `Сума чисел дорівнює - ${sum}`;
};
