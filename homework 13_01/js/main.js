function sumDigitsOfANumber(){
	let x = document.getElementById('number').value;
	let sum = 0;
	for (let i = 0; i < x.length; i++) {
		sum += Number(x[i]);
	}
	document.getElementById('resultOne').innerHTML = `Сума чисел дорівнює - ${sum}`;
	return sum;
};
function digitsOfANumberMirror(a) {
 return a === '' ? '' : digitsOfANumberMirror(a.slice(1)) + a[0];
};
function mirror(){
	let x = document.getElementById('numberDigits').value;
	let b = digitsOfANumberMirror(x);
	document.getElementById('resultTwo').innerHTML = b;
};
function letterSearch(){
	let s = document.getElementById('string').value;
	let l = document.getElementById('letter').value;
	let numberofletters = s.split(l).length-1;
	document.getElementById('resultThree').innerHTML = `Кількість "${l}" в цоьму рядку - ${numberofletters}`;
};
