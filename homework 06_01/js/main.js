function checkRange(){
	let numberRange = document.getElementById('numberOne').value;
	switch (Number(numberRange)){
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
			document.getElementById('resultOne').innerHTML = "Число в сірому діапазоні";
			break;
		case 8:
		case 9:
		case 10:
			document.getElementById('resultOne').innerHTML = "Число в сірому та червоному діапазоні";
			break;
		case 11:
		case 12:
		case 13:
			document.getElementById('resultOne').innerHTML = "Число в сірому, червоному та синьому діапазоні";
			break;
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
			document.getElementById('resultOne').innerHTML = "Число в червоному та синьому діапазоні";
			break;
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
		case 31:
			document.getElementById('resultOne').innerHTML = "Число в синьому діапазоні";
			break;
		default:
			document.getElementById('resultOne').innerHTML = "Це число не входить до будь-якого діапазону";
	}
};
function maximumNumber(){
	let num0 = +document.getElementById('number1').value;
	let num1 = +document.getElementById('number2').value;
	let num2 = +document.getElementById('number3').value;
	if (num0 > num1 && num0 > num2) {
		document.getElementById('resultTwo').innerHTML = num0;
	}
	 else if (num1 > num0 && num1 > num2) {
		document.getElementById('resultTwo').innerHTML = num1;
	}
	else if (num2 > num0 && num2 > num1){
		document.getElementById('resultTwo').innerHTML = num2;
	}
	else {
		document.getElementById('resultTwo').innerHTML = "Числа повинні бути унікальними! Виявлено декілька найбільших чисел";
	}
};
function checkPairNumbers(){
	let pairNumbers = document.getElementById('numberThree').value;
	let str = '';
	for (let i = 2; i <= pairNumbers; i = i + 2) {
		str += i + " ";
	}
	document.getElementById('resultThree').innerHTML = str;
};
function parityСheck(){
	let numbers = document.getElementById('numberFour').value;
	if (numbers == 12 || numbers < 3){
		document.getElementById('resultFour').innerHTML = "Вітаю! Це зима";
	}
	else {
		document.getElementById('resultFour').innerHTML = "Такого місяця не існує";
	}
	if (numbers > 2 && numbers < 6) {
		document.getElementById('resultFour').innerHTML = "Вітаю! Це весна";
	}
	if (numbers > 5 && numbers < 9) {
		document.getElementById('resultFour').innerHTML = "Вітаю! Це літо";
	}
	if (numbers > 8 && numbers < 12) {
		document.getElementById('resultFour').innerHTML = "Вітаю! Це осінь";
		}
};
function largestNumber(){
	let a = +document.getElementById('taskFiveA').value;
	let b = +document.getElementById('taskFiveB').value;
	let c = +document.getElementById('taskFiveC').value;
	let d = +document.getElementById('taskFiveD').value;
	let e = +document.getElementById('taskFiveE').value;
	let f = +document.getElementById('taskFiveF').value;
	let g = +document.getElementById('taskFiveG').value;
	let h = +document.getElementById('taskFiveH').value;
	let i = +document.getElementById('taskFiveI').value;
	let j = +document.getElementById('taskFiveJ').value;
	a > b && a > c && a > d && a > e && a > f && a > g && a > h && a > i && a > j ?
		document.getElementById('resultFive').innerHTML = a:
	b > a && b > c && b > d && b > e && b > f && b > g && b > h && b > i && b > j ?
		document.getElementById('resultFive').innerHTML = b:
	c > a && c > b && c > d && c > e && c > f && c > g && c > h && c > i && c > j ?
		document.getElementById('resultFive').innerHTML = c:
	d > a && d > b && d > c && d > e && d > f && d > g && d > h && d > i && d > j ?
		document.getElementById('resultFive').innerHTML = d:
	e > a && e > b && e > c && e > d && e > f && e > g && e > h && e > i && e > j ?
		document.getElementById('resultFive').innerHTML = e:
	f > a && f > b && f > c && f > d && f > e && f > g && f > h && f > i && f > j ?
		document.getElementById('resultFive').innerHTML = f:
	g > a && g > b && g > c && g > d && g > e && g > f && g > h && g > i && g > j ?
		document.getElementById('resultFive').innerHTML = g:
	h > a && h > b && h > c && h > d && h > e && h > f && h > g && h > i && h > j ?
		document.getElementById('resultFive').innerHTML = h:
	i > a && i > b && i > c && i > d && i > e && i > f && i > g && i > h && i > j ?
		document.getElementById('resultFive').innerHTML = i:
	j > a && j > b && j > c && j > d && j > e && j > f && j > g && j > h && j > i ?
		document.getElementById('resultFive').innerHTML = j:
		document.getElementById('resultFive').innerHTML = "Числа повинні бути унікальними! Виявлено декілька найбільших чисел";
};
function daysCheck(){
	let km = +document.getElementById('numberKM').value;
	let k = +document.getElementById('numberK').value;
	let d = 0;
	while (km < 50) {
		km += km*k/100;
		d++
		console.log(km);
	}
	document.getElementById('resultSix').innerHTML = `Норма пробігу спортсмена стала більше 50 км за ${d} день(-ів)`;
};
